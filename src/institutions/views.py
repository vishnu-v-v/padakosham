from django.shortcuts import render
from django.http import JsonResponse
from .models import *

def retrieve(request):
    data = request.GET.keys()  
    out = []
    if("district" in data):
        for i in SubDistrict.objects.all().filter(edu_district__district__name = request.GET["district"]):
            out.append(i.name)
        return JsonResponse({"SubDistricts" : list(set(out))} )
    elif("subdistrict" in data):
        for i in Insititution.objects.all().filter(sub_district__name = request.GET["subdistrict"]):
            out.append(i.name)
        return JsonResponse({"Schools" : list(set(out))})
    else:
        for i in District.objects.all():
            out.append(i.name)
        return JsonResponse({"Districts" : list(set(out))})
