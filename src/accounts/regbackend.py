from .models import *
from .forms import *
from django.shortcuts import get_object_or_404
def user_created(sender, user, request, **kwargs):
    form = UserForm(request.POST)
    data = RPProfile(user=user)
    data.institution = get_object_or_404(Institution,code=form.data["school_code"])
    data.save()

from registration.signals import user_registered
user_registered.connect(user_created)
